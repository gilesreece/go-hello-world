FROM golang:1.10

RUN mkdir /app

WORKDIR /app

COPY . .

RUN echo "nobody:x:65534:65534:Nobody:/:" > /etc_passwd

RUN CGO_ENABLED=0 go build -o /app/go-hello-world

FROM scratch

COPY --from=0 /app/go-hello-world /go-hello-world

COPY --from=0 /etc_passwd /etc/passwd

USER nobody

CMD ["/go-hello-world"]
